/*
DROP TABLE IF EXISTS `test`;

CREATE TABLE `test` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `string` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bool` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `test` (string, bool) VALUES ('Berlin',0),('Budapest',0);
*/